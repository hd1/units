# -*- coding: utf-8 -*-
import hashlib
import json
import os
import os.path
import threading
import unittest
import urlparse

import main
import requests


class UnitsTest(unittest.TestCase):

    def test_tts(self):
        TEXT = 'hello world'
        URL = 'http://units.d8u.us/tts'
        params = {'text': TEXT}
        response = requests.get(URL, params=params).json()
        assertFalse(any(not d.has_key(k) for k in ['url', 'text', 'language_code', 'instructions']))

        # generating worked, now try retrieivng the file
        url = response['url']
        parsed = urlparse.urlparse(url)

        filename = parsed[[1:response.rindex('.mp3')]
        params = {'retrieving': filename}
        response = requests.get(URL, params=params, stream=True)
        assert(response.headers['Content-Type'] == 'audio/mpeg')
        onlyfiles = [f for f in os.listdir(mypath) if os.isfile(os.path.join('/home','ubuntu','public_html', f))]
        assert(parsed.path[1:] in onlyfiles)

    def test_today(self):
        DATE = '20151214'
        response = main.Today().GET(DATE)
        self.assertEquals(len(response), 1)
        self.assertTrue(response[0].has_key('starts') and response[0].has_key('title'))

    def test_new_paste(self):
        print 'New paste'
        data = {'content': 'I love Lean Liam'}
        id_ = hashlib.sha224(data['content']).hexdigest()
        response = requests.post('http://units.d8u.us/paste', data=data).json()
        self.assertEquals(response['id'], id_)
        self.assertTrue(response.has_key('id') and response.has_key('expiration') and response.has_key('saved'))

    def test_paste(self):
        print 'Verifying that the new paste is stored'
        paste_id = '5676aaa0944af96f1692febe7e3350614fa95a71fede632624cb546f'
        response = requests.get('http://units.d8u.us/paste/{0}'.format(paste_id)).json()
        self.assertEquals(response['id'], paste_id)

    def test_app(self):
        print 'Appstore search test'
        response = requests.get('http://units.d8u.us/app/tasker').json()
        EXPECTED = ['category', 'rating', 'name', 'price', 'platform', 'applink', 'link', 'query', 'rating count', 'description']
        self.assertTrue([k in response[0].keys() for k in EXPECTED])

    def test_soccer(self):
        print 'Soccer'
        response = requests.get('http://units.d8u.us/soccer/newcastle%20united/').json()
        EXPECTED = ['home', 'away', 'time']
        self.assertTrue([e in response.keys() for e in EXPECTED])

    def test_tv(self):
        print 'TV'
        try:
            response = requests.get('http://units.d8u.us/tv/the+good+wife').json()
        except ValueError:
            response = {}
        EXPECTED = ['title', 'torrent', 'link']
        self.assertTrue(response == {} or all([k in response.keys() for k in EXPECTED]))

    def test_weather(self):
        print 'Weather'
        response = requests.get('http://units.d8u.us/weather/94920').json()
        EXPECTED_KEYS = ['epoch time', 'precipitation chance', 'temperature', 'nearest storm distance', 'cloud cover', 'visibility', 'summary', 'ozone', 'location', 'barometric pressure', 'apparent temperature', 'humidity', 'wind speed']
        for k in response.keys():
            if k.endswith('bearing'):
                continue
            self.assertTrue(k in EXPECTED_KEYS)

    def test_sales(self):
        print 'Sales'
        response = requests.get('http://units.d8u.us/tax/10/8/').json()
        expected = {"Total": 10.8, "Subtotal": 10.0, "Tax Rate": 1.08}
        self.assertEquals(response, expected)

    def test_holiday(self):
        print 'Holiday'
        response = requests.get('http://units.d8u.us/holiday').json()
        self.assertTrue(response.has_key('Date') and response.has_key('Day Off'))

    def test_apod(self):
        print 'Apod'
        response = requests.get('http://units.d8u.us/nasa').json()
        self.assertTrue([r.has_key('date') and r.has_key('title') and r.has_key('link') and r.has_key('image') for r in response])

    def test_ebay(self):
        print 'eBay'
        response = requests.get('http://units.d8u.us/ebay/usb').json()
        self.assertTrue([r.has_key('price') and r.has_key('link') and r.has_key('title') and r.has_key('endTime') and r.has_key('location') for r in response])

    def test_github(self):
        print 'github'
        response = requests.get('http://units.d8u.us/code/activerecord?repo=rails/rails&compression=1').json()
        self.assertTrue([r.has_key('url') and r.has_key('path') and r.has_key('url') for r in response])

    def test_compression(self):
        print 'compression'
        response1 = requests.get('http://units.d8u.us/unit/kg/lb/1').json()
        response2 = requests.get('http://units.d8u.us/unit/kg/lb/1?compression=true').json()
        self.assertEquals(response1, response2)

    def test_url_regex(self):
        print 'trailing slash test'
        response1 = requests.get('http://units.d8u.us/unit/kg/lb/1').json()
        response2 = requests.get('http://units.d8u.us/unit/kg/lb/1/').json()
        self.assertEquals(response1, response2)

    def test_iata(self):
        print 'IATA'
        response = requests.get('http://units.d8u.us/airport/LHR').json()
        self.assertEquals(response['airport'],'Heathrow')

    def test_time(self):
        print 'Timezone'
        response = requests.get('http://units.d8u.us/time/PST8PDT/Israel/20151102111109').json()
        self.assertEquals(response['Desired Time'], "Mon Nov  2 13:11:09 2015")

    def test_unit(self):
        print 'Unit Conversion'
        response = requests.get('http://units.d8u.us/unit/kg/lb/1').json()
        self.failUnlessAlmostEqual(float(response['Amount Desired']),2.2, places=1)

    def test_currency(self):
        print 'Currency Conversion'
        response = requests.get('http://units.d8u.us/unit/USD/USD/1').json()['Amount Desired']
        self.failUnlessAlmostEqual(float(response),1,places=0)

    def test_location(self):
        print 'POIs'
        response = requests.get('http://units.d8u.us/poi/Tiburon, CA/1').json()
        self.assertEquals(len(response), 1)

    def test_word(self):
        print 'Dictionary'
        response = requests.get('http://units.d8u.us/word/kind').json()
        self.assertTrue([k in ['definition', 'word', 'synonyms', 'antonyms', 'source'] for k in response.keys()] or response.has_key('error'))

    def test_summary(self):
        print 'Summary'
        response = requests.get('http://units.d8u.us/summary/?url=http://fivethirtyeight.com/features/the-republican-establishment-inches-toward-marco-rubio/').json()
        self.failUnless((response.has_key('url') and response.has_key('sumy_summary')))
        self.failIf(response.has_key('error'), 'Error returned in obtaining summary')

    def test_stock(self):
        print 'Stock'
        response = requests.get('http://units.d8u.us/quote/').json()
        self.failUnless(response.has_key('Company'))

    def test_temperature(self):
        print 'Temperature'
        response = requests.get('http://units.d8u.us/unit/tempC/tempF/0').json()
        self.failUnlessAlmostEqual(float(response['Amount Desired']), 32.0, 0)

    def test_edmunds(self):
        print 'Edmunds'
        response = requests.get('http://units.d8u.us/vin/1N4AL3AP4DC295509').json()
        valid_vin = response.has_key('VIN') and response.has_key('Edmunds TMV®') and response.has_key('Total Price') and response.has_key('Disclaimer') and response.has_key('Copyright')
        invalid_vin = response.has_key('Endpoint') and response.has_key('error')
        self.assertTrue(valid_vin or invalid_vin)

if __name__ == '__main__':
    unittest.main()
